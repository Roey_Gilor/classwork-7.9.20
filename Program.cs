﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork_2._9._20
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDictionaryIntToString map_id_to_name = new MyDictionaryIntToString();
            map_id_to_name.Add(1, "danny");
            map_id_to_name.Add(2, "moshe");

            //map_id_to_name.Add(3, "suzi");
            KeyValuePair<int, string> keyValuePair_3_suzi = new KeyValuePair<int, string>(3, "rani");
            map_id_to_name = map_id_to_name + keyValuePair_3_suzi;
            // i = i + 1;
            map_id_to_name = map_id_to_name + new KeyValuePair<int, string>(5, "suzi");

            // i = i - 1;
            map_id_to_name = map_id_to_name - 5; // remove item with key=5 (if found)


            MyDictionaryIntToString map_id_to_name2 = new MyDictionaryIntToString();
            map_id_to_name2 = map_id_to_name2 + new KeyValuePair<int, string>(3, "rani");
            map_id_to_name2 = map_id_to_name2 + new KeyValuePair<int, string>(8, "shmulik");

            // 1: (1, danny) (2, moshe) (3, rani)
            // 2: (3, rani) (8, shmulik)
            // result = (1, danny) (2, moshe) (3, rani)  (8, shmulik)
            MyDictionaryIntToString map_id_to_name_merge = map_id_to_name + map_id_to_name2;

            // 1: (1, danny) (2, moshe) (3, rani)
            // 2: (3, rani) (8, shmulik)
            // result = (1, danny) (2, moshe) 
            MyDictionaryIntToString map_id_to_name_cut = map_id_to_name - map_id_to_name2;

        }
    }
}
