﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork_2._9._20
{
    class MyDictionaryIntToString
    {
        private List<int> keys;
        private List<string> values;
        public MyDictionaryIntToString()
        {
            keys = new List<int>();
            values = new List<string>();
        }
        public void Add (int key, string value)
        {
            if (ContainsKey(key))
                throw new Exception("Key is already exist");
            keys.Add(key);
            values.Add(value);
        }
        public bool TryAdd(int key, string value)
        {
            if (ContainsKey(key))
                return false;
            keys.Add(key);
            values.Add(value);
            return true;
        }
        public string Get (int key)
        {
            if (!ContainsKey(key))
                throw new Exception("Key is not exist");
            return values[keys.IndexOf(key)];
        }
        public bool TryGet(int key, out string value)
        {
            if (ContainsKey(key))
            {
                value = values[keys.IndexOf(key)];
                return true;
            }
            value = null;
            return false;
        }
        public void AddOrOverride(int key, string value)
        {
            if (Get(key) != null)
                values[keys.IndexOf(key)] = value;
            else
                Add(key, value);
        }
        public void Clear()
        {
            keys.Clear();
            values.Clear();
        }
        public bool ContainsKey (int key)
        {
            return keys.Contains(key);
        }
        public bool ContainsValue (string value)
        {
            return values.Contains(value);
        }
        public int CountItemsWithThisValue(string value)
        {
            int count = 0;
            foreach (string item in values)
            {
                if (item == value)
                    count++;
            }
            return count;
        }
        public List<int> GetKeys()
        {
            List<int> replicia = new List<int>();
            for (int i = 0; i < keys.Count; i++)
            {
                replicia[i] = keys[i];
            }
            return replicia;
        }
        public List<string> GetValues()
        {
            List<string> replicia = new List<string>();
            for (int i = 0; i < values.Count; i++)
            {
                replicia[i] = values[i];
            }
            return replicia;
        }
        public static MyDictionaryIntToString operator +(MyDictionaryIntToString me, KeyValuePair<int, string> item)
        {
            me.TryAdd(item.Key, item.Value);
            return me;
        }
        public static MyDictionaryIntToString operator -(MyDictionaryIntToString me, int key)
        {
            if (me.TryGet(key, out string value))
            {
                me.values.RemoveAt(me.keys.IndexOf(key));
                me.keys.RemoveAt(me.keys.IndexOf(key));
            }
            return me;
        }
        public static MyDictionaryIntToString operator +(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            MyDictionaryIntToString newDic = new MyDictionaryIntToString();
            foreach (int key in mydic1.keys)
            {
                newDic.Add(key, mydic1.Get(key));
            }
            foreach (int key in mydic2.keys)
            {
                newDic.TryAdd(key, mydic2.Get(key));
            }
            return newDic;
        }
        public static MyDictionaryIntToString operator -(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            MyDictionaryIntToString newDic = new MyDictionaryIntToString();
            foreach (int key in mydic1.keys)
            {
                newDic.Add(key, mydic1.Get(key));
            }
            foreach (int key in mydic2.keys)
            {
                newDic = mydic1 - key;
            }
            return newDic;
        }
        public static bool operator ==(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            if (mydic1 == null && mydic2 == null)
                return true;
            if (mydic1 == null || mydic2 == null)
                return false;
            if (mydic1.keys.Count != mydic2.keys.Count)
                return false;
            foreach (int key in mydic1.keys)
            {
                if (!mydic2.ContainsKey(key))
                    return false;
                if (mydic1.Get(key) != mydic2.Get(key))
                    return false;
            }
            return true;
        }
        public static bool operator !=(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            return !(mydic1 == mydic2);
        }
        public static bool operator >(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            if (mydic1 == null || mydic2 == null)
                return false;
            return mydic1.keys.Count > mydic2.keys.Count;
        }
        public static bool operator <(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            if (mydic1 == null || mydic2 == null)
                return false;
            return mydic1.keys.Count < mydic2.keys.Count;
        }
        public override bool Equals(object obj)
        {
            return this == (MyDictionaryIntToString)obj;
        }
        public override string ToString()
        {
            string str = "";
            foreach (int key in keys)
            {
                str += $"[{key} , {Get(key)}] \n";
            }
            return str;
        }
    }
}
